create database igreja;

use igreja;

create table evento
(
    id         bigint unsigned auto_increment
    primary key,
    title      varchar(191) null,
    descricao  text         null,
    data       date         null,
    hora       time         null,
    enabled    bit          default 1,
    created_at timestamp    default CURRENT_TIMESTAMP,
    updated_at timestamp    default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
    limite     int          null
)
    collate = utf8mb4_unicode_ci;

create table agendamento
(
    id         bigint unsigned auto_increment
    primary key,
    evento_id bigint unsigned not null,
    FOREIGN KEY(evento_id) REFERENCES evento(id) on DELETE CASCADE,
    nome       varchar(191) null,
    celular    varchar(191) null,
    cpf        varchar(191) null,
    created_at timestamp    default CURRENT_TIMESTAMP,
    updated_at timestamp    default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
    data_nascimento date null,
    agendamento_id_pai bigint unsigned null,
    FOREIGN KEY (agendamento_id_pai) REFERENCES agendamento(id) ON DELETE CASCADE
)
    collate = utf8mb4_unicode_ci;

INSERT INTO evento (title, descricao, data, hora, enabled, created_at, updated_at, limite) VALUES ('Sábado', null, '2027-12-19', '19:00:00', 1, '2020-12-18 17:52:26', '2020-12-18 17:52:27', 2);
INSERT INTO evento (title, descricao, data, hora, enabled, created_at, updated_at, limite) VALUES ('Domingo', null, '2027-12-20','09:00:00', 1, '2020-12-18 17:53:01', '2020-12-18 17:53:03', 6);
