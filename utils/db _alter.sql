-- source utils/db _alter.sq
USE igreja;
ALTER TABLE agendamento ADD data_nascimento date is null;
ALTER TABLE agendamento ADD agandamento_id bigint is null CONSTRAINT agandamento_id FOREIGN KEY (id) REFERENCES agendamento (id);