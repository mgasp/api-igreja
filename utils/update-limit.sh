#!/bin/bash
NEW_LIMIT=$1

if [ -z "$NEW_LIMIT" ]
then
	echo "Usage: update-limit <NEW_LIMIT>"
	exit 1;
fi

mysql -u root -e "update igreja.evento set evento.limite=$NEW_LIMIT;"
