#!/bin/bash
MYSQL_USER=root
MYSQL_PASS=PASSWORD123

mysql_is_running () {
  nc -zvv localhost 3306 -w 1 2>/dev/null 1>&2
  echo $?
}

echo "Starting mysql container (this may take a while) ... "

docker rm -f mysql 2>/dev/null 1>&2
docker run -d --network host -e MYSQL_ROOT_PASSWORD="$MYSQL_PASS" --name mysql mysql:5.7 2>/dev/null 1>&2

docker cp utils/db.sql mysql:/

while [ "$(mysql_is_running)" != "0" ]
do
  sleep 1
done;



docker exec mysql mysql -u root -p$MYSQL_PASS -e "source /db.sql" 2>/dev/null 1>&2
echo "done!"

