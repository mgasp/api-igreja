const mysql = require('mysql');
const intl = require('intl');

const TABLE_EVENT_NAME = 'evento';
const TABLE_SCHEDULING_NAME = 'agendamento';

class DatabaseConnector {
  constructor() {
    this.host = null;
    this.user = null;
    this.password = null;
    this.database = null;
    this.connection = null;
  }

  async init (host, user, password, database) {
    try {
      this.host = host;
      this.user = user;
      this.password = password;
      this.database = database;
      console.log('Database started...');
      this.connection = mysql.createConnection({
        host     : this.host,
        user     : this.user,
        password : this.password,
        database : this.database
      });

      this.connection.connect();
    } catch (error) {
      throw error;
    }
  }

  async query (query) {
      return new Promise ((resolve,reject) => {
        try {
          this.connection.query(query, function (error, results, fields) {
            if (error) throw error;
            resolve(results);
          });
        } catch (error) {
          reject(error);
        }
      });
  }

  async stop () {
    try {
      if (this.connection) {
        this.connection.end();
        this.connection = null;
      }
    } catch (error) {
      throw error;
    }
  }

  async add(params) {
    try {
      const { evento_id,form } = params;

      if (((typeof evento_id === 'number') && evento_id >= 0)
        && (form && (typeof form === 'object'))) {
        const { nome, celular, cpf, dependentes } = form;

        const _celular = celular.replace(/\D/g,'');
        const _cpf = cpf.replace(/\D/g,'');

        const isValidEventId = await this.isValidEventId(evento_id);

        if (isValidEventId && nome && celular && cpf) {
          const parentQuery = 'INSERT into ' + TABLE_SCHEDULING_NAME
          + ' (evento_id, nome,celular,cpf)'
          + ' VALUES('+ evento_id + ',"' + nome + '", "' + _celular + '", "'
          + _cpf + '")';

          const parentQueryResult = await this.query(parentQuery);

          // console.log(parentQueryResult);

          if (!parentQueryResult) return false;

          const { insertId: parentAgendamentoId } = parentQueryResult;

          if ((typeof(dependentes) === 'object') && (typeof(dependentes.forEach === 'function'))) {
            dependentes.forEach(async (dependente) => {
              const { nome: _nome, nascimento } = dependente;

              const query = 'INSERT into ' + TABLE_SCHEDULING_NAME
              + ' (evento_id, nome, data_nascimento, agendamento_id_pai)'
              + ' VALUES('+ evento_id + ',"' + _nome + '", "' + nascimento + '", "'
              + parentAgendamentoId + '")';

              await this.query(query);
            });

          }
          // const parentQueries = [];
          // return this.query(query);
          return parentQueryResult;
        }

        return false;
      }
    } catch (error) {
      console.log('Error when adding ', error);
      throw error;
    }
  }

  async remove(id) {
    try {
      if (!id) {
        return false;
      }

      const query = 'DELETE from ' + TABLE_SCHEDULING_NAME + ' WHERE id = '
        + id;

      return this.query(query);
    } catch (error) {
      throw error;
    }
  }

  async add_event(params) {
    try {
      const { form } = params;

      if (typeof form === 'object') {
        const { title, data, horario, limite, enabled } = form;

        if (title && data && horario && limite) {
          const query = 'INSERT into ' + TABLE_EVENT_NAME
            + ' (title, data, hora, limite, enabled)'
            + ' VALUES("'+ title + '","' + data + '", "' + horario + '", "'
            + limite + '",' + enabled + ')';

          return this.query(query);
        }

        return false;
      }
    } catch (error) {
      console.log('Error when adding ', error);
      throw error;
    }
  }

  async remove_event(id) {
    try {
      if (!id) {
        return false;
      }

      const query = 'DELETE from ' + TABLE_EVENT_NAME + ' WHERE id = '
        + id;

      return this.query(query);
    } catch (error) {
      throw error;
    }
  }

  async getAllEventData() {
    try {
      const query = 'SELECT * from ' + TABLE_EVENT_NAME + ' where enabled = 1';
      const data = await this.query(query);

      await Promise.all(data.map(async (event) => {
        const schedulingQuery = 'SELECT * from ' + TABLE_SCHEDULING_NAME
          + ' WHERE evento_id = ' + event.id;

        const schedulings = await this.query(schedulingQuery);

        schedulings.forEach( (schedule) => {
          if (schedule.cpf) {
            schedule.cpf = this.addCpfChars(schedule.cpf);
          }

          if (schedule.celular) {
            schedule.celular = this.addPhoneChars(schedule.celular);
          }
        });
        event.items = schedulings;
        event.data = this.formatDate(event.data);
        event.hora = this.formatHour(event.hora);
        event.horario = this.formatHour(event.hora);
      }));

      return this.filterData(data);
    } catch (error) {
      throw error;
    }
  }

  async filterData (data) {
    try {
      const todayDate =
        new Date().toLocaleDateString("pt-BR", {timeZone: "America/Sao_Paulo"});

      const filteredData =  data.filter(this.isNotOldEvent.bind(this,todayDate));
      return  filteredData;
    } catch (error) {
      throw error;
    }
  }

  isNotOldEvent (todayDate, event) {
    try {
      const { data } = event;

      if (!data || !todayDate) return false;

      const sData = data.split('/');
      const _data = new Date(sData[2], sData[1], sData[0]);

      const sTodayDate = todayDate.split('/');
      const _todayDate = new Date(sTodayDate[2],sTodayDate[1],sTodayDate[0])

      return (_data >= _todayDate);
    } catch (error) {
      throw error;
    }
  }

  async isValidEventId (eventId) {
    try {
      const queryEvent = 'SELECT limite from ' + TABLE_EVENT_NAME
        + ' where id = ' + eventId;

      const queryEventResult = await this.query(queryEvent);
      const limit = (typeof queryEventResult === 'object')
        && (queryEventResult[0]) ? queryEventResult[0].limite
        : 0;

      if (!limit)  {
        return false;
      }

      const querySchedulings = 'SELECT COUNT(*) from ' + TABLE_SCHEDULING_NAME
        + ' WHERE evento_id = ' + eventId;

      const querySchedulingsResult = await this.query(querySchedulings);

      const schedulesCounter = (typeof querySchedulingsResult  === 'object')
        && querySchedulingsResult[0] && querySchedulingsResult[0]['COUNT(*)']
        ? querySchedulingsResult[0]['COUNT(*)'] : 0

      return schedulesCounter < limit;
    } catch (error) {
      throw error;
    }
  }

  addCpfChars (cpf) {
    try {
      return cpf.slice(0,3) + '.' + cpf.slice(3,6) + '.'
        + cpf.slice(6,9) + '-' + cpf.slice(9,11);
    } catch (error) {
      throw error;
    }
  }

  addPhoneChars (phoneNumber) {
    try {
      // return '(' + phoneNumber.slice(0,2) + ')' + ' ' + phoneNumber.slice(2,7)
      //   + '-' + phoneNumber.slice(7,12);
      return '(**)' + ' ' + '****'
        + '-**' + phoneNumber.slice(9,12);
    } catch (error) {
      throw error;
    }
  }

  formatDate(dateString) {
    try {
      if (!dateString) {
        return '';
      }

      const date = new Date(dateString);

      const month = (date.getMonth() + 1)
      const finalMonth = month > 9 ? month : '0' + month;

      return '' + date.getDate() + '/' + finalMonth  + '/'
        + date.getFullYear();
    } catch (error) {
      throw error;
    }
  }

  formatHour(hourString) {
    try {
      if (!hourString) {
        return '';
      }

      return hourString.split(':',2).join(':');
    } catch (error) {
      throw error;
    }
  }
}

module.exports = DatabaseConnector;
