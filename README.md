# simple api for igreja

## Install steps (debian-based systems)
### 1 Install deps
```bash
sudo apt-get install npm nodejs mysql-server

```

### 2 Init database
```sql
  cd api-igreja
  sudo mysql -u root
  source utils/db.sql

  # REPLACE USER and PASSWORD with your desired credentials
  create user 'USER'@'localhost' identified with mysql_native_password BY 'PASSWORD';
  grant all privileges on igreja.* to 'USER'@'localhost';

```

### 3 Install node dependencies
```
  cd api-igreja
  npm install
```

### 4 Run
```
  env API_IGREJA_DB_USER=USER API_IGREJA_DB_PASS=PASSWORD npm start
```
This will start application on default URL: http://localhost:3000/api/public/api

### 5 Enviroment variables
```
API_IGREJA_DB_HOST
API_IGREJA_DB_USER
API_IGREJA_DB_PASS
API_IGREJA_DATABASE
API_IGREJA_LISTEN_PORT
API_IGREJA_DEFAULT_ROUTE_PATH
```

### 6 Setting up apache2
Install apache and enable modules:
```
sudo apt-get install apache2
sudo a2enmod proxy
sudo a2enmod proxy_http
```

Open the file `/etc/apache2/sites-available/000-default.conf`:
```
sudo vi /etc/apache2/sites-available/000-default.conf
```

And and add before `</VirtualHost>` the following commands (replace the
`10.0.3.150` with your IP address):
```
ProxyPreserveHost On
ProxyPass /api http://localhost:3000/api
ProxyPassReverse /api http://localhost:3000/api

ProxyPass / http://10.0.3.150:8080/
ProxyPassReverse / http://10.0.3.150:8080/
```

Restart apache2
```
sudo service apache2 restart
```